package com.theendcomplete

import grails.rest.Resource

@Resource()
class Department {
    String name


    static hasMany = [employers: Employer, usrOrders: UsrOrder]

    static constraints = {
        name(nullable: false)
    }

    @Override
    String toString() {
        return name
    }
}
