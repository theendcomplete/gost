package com.theendcomplete

import grails.rest.Resource

@Resource()
class Item {
    Department type
    String name

    static constraints = {
        type(nullable: true)
        name(nullable: false)
    }
    @Override
    String toString() {
        return name
    }
}
