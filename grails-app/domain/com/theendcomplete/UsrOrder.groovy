package com.theendcomplete

import grails.rest.Resource

@Resource()
class UsrOrder {
    Date dateCreated
    Date deadLine
    String status
    def worker

//    static hasOne = [department: Department]
    static hasMany = [items: Item]

    static constraints = {
        worker(nullable: true)
        dateCreated(nullable: true)
        deadLine(nullable: true)
    }

    @Override
    String toString() {
        return id
    }
}
