package com.theendcomplete

import grails.rest.Resource

@Resource()
class Employer {
    String name


    static belongsTo = [Department, UsrOrder]
    static constraints = {
        name(nullable: false)
//        department (nullable: false)
//        order (nullable: false)
    }

    @Override
    String toString() {
        return name
    }
}

