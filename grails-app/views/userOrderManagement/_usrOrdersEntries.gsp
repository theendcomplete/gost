<asset:javascript src="jquery.js"/>

<div id="usrOrder${it.getId()}" class="well">

    <div class="h3">Заказ № ${it.getId()}</div>

    <div id="deadline" class="h4">
        deadline: <g:formatDate format="dd-MM-yyyy HH:mm" date="${it.deadLine}"/>
    </div>
    %{--<div id="order${it.getId()}text">--}%
    %{--${it.text}  <br>--}%
    %{--</div>--}%

    <div id="worker" class="h4">
        Исполнитель:${it.worker}
    </div>

    %{--<div id="popover_quote${it.getId()}" style="display: none">--}%
    %{--<div>--}%
    %{--${it.text}--}%
    %{--<g:render template="bookList" model="[data: author.bookList]"/>--}%
    %{--</div>--}%
    %{--</div>--}%
    %{--<g:render template="two" model="${[time: time]}"/>--}%

    <div id="createdBy" class="text-right">
        created at <g:formatDate format="dd-MM-yyyy HH:mm" date="${it.dateCreated}"/>
    </div>




    %{--<div id="rating${it.getId()}">--}%
    %{--rating: ${it.rating}<br>--}%
    %{--</div>--}%

    %{--<div class="btn-group-sm">--}%
    %{--<button type="submit" class="btn btn-success" id="like" onclick="like('${it.getId()}');">+</button>--}%
    %{--<button type="submit" class="btn btn-danger" id="dislike" onclick="dislike('${it.getId()}');">-</button>--}%
    %{--<sec:ifAllGranted roles="ROLE_ADMIN">--}%
    %{--<a href="${createLink(controller: 'quotes', action: 'removeQuote', id: it.getId())}"--}%
    %{--class="btn"--}%
    %{--role="button">Delete</a>--}%

    %{--<a href="${createLink(controller: 'quotes', action: 'removeQuote', id: it.getId())}"--}%
    %{--class="btn"--}%
    %{--role="button">Edit</a>--}%
    %{--</sec:ifAllGranted>--}%
    %{--</div>--}%
</div>


%{--<script>--}%
%{--var quoteText = document.getElementById("quote${it.getId()}text");--}%

%{--quoteText.style.cursor = 'pointer';--}%
%{--quoteText.onclick = function () {--}%
%{--$('form[data-update-target]').live('ajax:success', function (evt, data) {--}%
%{--var target = $(this).data('quote${it.getId()}"');--}%
%{--$('#' + target).html(data);--}%
%{--});--}%
%{--}--}%
%{--)--}%
%{--;--}%
%{--</script>--}%

%{--<script>--}%
%{--var quoteText = document.getElementById("quote${it.getId()}text");--}%
%{--quoteText.style.cursor = 'pointer';--}%
%{--quoteText.onclick = function () {--}%
%{--$('#quote${it.getId()}').popover('show')--}%
%{--window.alert("Here will be redirect to quote detail page")--}%
%{--location.href = "${createLink(controller: 'quotes', action: 'show',id:"${it.getId()}" )}"--}%
%{--};--}%
%{--</script>--}%
%{--<script>--}%
%{--$(function () {--}%
%{--$("[data-toggle='popover']").each(function (index) {--}%
%{--var contentId = $(this).data('contentId');--}%
%{--var contentElement = $("#" + contentId);--}%

%{--$(this).popover({--}%
%{--html: true,--}%
%{--trigger: 'click',--}%
%{--content: function () {--}%
%{--return contentElement.html();--}%
%{--})--}%
%{--});--}%

%{--});--}%

%{--</script>--}%


%{--<script>--}%
%{--var quoteText = document.getElementById("quote${it.getId()}text");--}%
%{--quoteText.style.cursor = 'pointer';--}%
%{--quoteText.onclick = function () {--}%
%{--$(this).popover({--}%
%{--html: true,--}%
%{--trigger: 'click',--}%
%{--content: "Votes: \n ${it.likes.groupBy({it.dateCreated})}"--}%
%{--})--}%
%{--}--}%
%{--$('#quote${it.getId()}').popover('show')--}%
%{--window.alert("Here will be redirect to quote detail page")--}%
%{--location.href = "${createLink(controller: 'quotes', action: 'show',id:"${it.getId()}" )}"--}%
%{--}--}%
%{--;--}%
%{--</script>--}%


