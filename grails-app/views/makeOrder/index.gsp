<!doctype html>
<html>

<head>
    <meta name="layout" content="main"/>
    <title>Please create your account</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>
    <g:javascript library='jquery'/>
</head>

<body>

<form class="form-horizontal" action="/makeOrder/create" method="POST">
    <fieldset>

        <!-- Form Name -->
        <legend>Создать заказ</legend>

        %{--<!-- Select Basic -->--}%
        %{--<div class="form-group">--}%
        %{--<label class="col-md-4 control-label" for="department">Категория мебели</label>--}%

        %{--<div class="col-md-4">--}%
        %{--<select id="department" name="department" class="form-control">--}%
        %{--<option value="1">Мягкая мебель</option>--}%
        %{--<option value="2">Офисная мебель</option>--}%
        %{--<option value="3">Системы хранения</option>--}%
        %{--</select>--}%
        %{--</div>--}%
        %{--</div>--}%
        <!-- Select Basic -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="department">Категория мебели</label>

            <div class="col-md-4">
                <g:select class="form-control" id="department.id" name="department.id" from="${departments}"
                          optionKey="id"
                          optionValue="name" noSelection="[null: 'Выберите тип мебели']"
                          onchange="departmentChange(this.value);"/>
            </div>
        </div>


        <!-- Select Basic -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="employer">Рабочий</label>

            <div class="col-md-4">

                <div>
                    %{--<b>Sub-Category: </b>--}%
                    <span id="workers"></span>
                </div>
                %{--<select id="employer" name="employer" class="form-control">--}%
                %{--<option value="1">Option one</option>--}%
                %{--<option value="2">Option two</option>--}%
                %{--</select>--}%
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="itemName">Что нужно сделать</label>

            <div class="col-md-4">
                <input id="itemName" name="itemName" type="text" placeholder="например, стул или диван"
                       class="form-control input-md" required="">
                <span class="help-block">Какой предмет необходимо изготовить</span>
            </div>
        </div>


        <div class="form-group">
            <label class="col-md-4 control-label" for="dateDeadline">Сделать до</label>

            <div class="col-md-4">
                <g:datePicker id="dateDeadline" name="dateDeadline" precision="day"></g:datePicker>

                <span class="help-block">Какой предмет необходимо изготовить</span>
            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="save">Сделать заказ</label>

            <div class="col-md-4">
                <button id="save" name="save" class="btn btn-primary">Сделать заказ</button>
            </div>
        </div>

    </fieldset>
</form>
<script>
    function departmentChange(categoryId) {
        <g:remoteFunction controller="makeOrder" action="departmentChange"
                    update="workers"
                    params="'departmentId='+categoryId"/>
    }
</script>
</body>
</html>