package gost

import com.theendcomplete.Department
import com.theendcomplete.Employer

class BootStrap {

    def init = { servletContext ->
        if (!Employer.findByName("Петрович")) {
            def employer = new Employer(name: "Петрович").save(flush: true)
            def employer2 = new Employer(name: "Ванька").save(flush: true)
            def employer3 = new Employer(name: "Сергей Анатольевич").save(flush: true)
            def employer4 = new Employer(name: "Лёха Серый").save(flush: true)
            def employer5 = new Employer(name: "Василий").save(flush: true)
            def employer6 = new Employer(name: "Петрович2").save(flush: true)
            def department = new Department(name: "Мягкая мебель").save(flush: true)
            def department2 = new Department(name: "Офисная мебель").save(flush: true)
            def department3 = new Department(name: "Системы хранения").save(flush: true)

            department.addToEmployers(employer)
            department.addToEmployers(employer2)
            department.addToEmployers(employer3)
            department2.addToEmployers(employer4)
            department2.addToEmployers(employer5)
            department3.addToEmployers(employer6)

        }
    }
    def destroy = {
    }
}
