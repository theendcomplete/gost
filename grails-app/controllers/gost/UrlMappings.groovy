package gost

class UrlMappings {

    static mappings = {
        "/api/$controller/$id"(parseRequest: true) {
            action = [GET: "show", PUT: "update", POST: "update", DELETE: "delete"]
            constraints {
                id matches: /\d+/
            }
        }
//            constraints {
//
//// RESTful list mapping
//        name restEntityList: "/$controller"(parseRequest: true) {
//            action = [GET: "list", POST: "save"]
//        }
//
//// RESTful entity mapping
//        name restEntity: "/$controller/$id"(parseRequest: true) {
//            action = [GET: "show", PUT: "update", POST: "update", DELETE: "delete"]
//            constraints {
//                id matches: /\d+/
//            }
//        }

//
        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(view: "/index")
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
