package com.theendcomplete

class UserOrderManagementController {

    def index() {
        def usrOrders = UsrOrder.list()
        [usrOrders: usrOrders]
    }

    def list() {
        def usrOrders = UsrOrder.list()
//        [usrOrders: usrOrders]
        def model = [usrOrders: usrOrders]


        render(view: 'index', model: model)
    }
}
