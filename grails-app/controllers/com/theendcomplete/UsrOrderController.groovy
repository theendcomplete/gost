package com.theendcomplete

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UsrOrderController {

//    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond UsrOrder.list(params), model:[usrOrderCount: UsrOrder.count()]
    }

    def show(UsrOrder usrOrder) {
        respond usrOrder
    }

    def create() {
        respond new UsrOrder(params)
    }

    @Transactional
    def save(UsrOrder usrOrder) {
        if (usrOrder == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (usrOrder.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond usrOrder.errors, view:'create'
            return
        }

        usrOrder.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'usrOrder.label', default: 'UsrOrder'), usrOrder.id])
                redirect usrOrder
            }
            '*' { respond usrOrder, [status: CREATED] }
        }
    }

    def edit(UsrOrder usrOrder) {
        respond usrOrder
    }

    @Transactional
    def update(UsrOrder usrOrder) {
        if (usrOrder == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (usrOrder.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond usrOrder.errors, view:'edit'
            return
        }

        usrOrder.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'usrOrder.label', default: 'UsrOrder'), usrOrder.id])
                redirect usrOrder
            }
            '*'{ respond usrOrder, [status: OK] }
        }
    }

    @Transactional
    def delete(UsrOrder usrOrder) {

        if (usrOrder == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        usrOrder.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'usrOrder.label', default: 'UsrOrder'), usrOrder.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'usrOrder.label', default: 'UsrOrder'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
