package com.theendcomplete

class MakeOrderController {

    def index() {
        def departmentList = Department.list()
        [departments: departmentList]
    }

    def create() {

        if (params.itemName) {
            Department dep = Department.get(params.department.id)
            def worker = dep
            if (params.worker.id) {
                worker = Employer.get(params.worker.id)
            }
            Item newItem = new Item(name: params.itemName, type: dep).save(flush: true)
            def usrOrder = new UsrOrder(worker: worker, deadLine: params.dateDeadline, status: "new").save()
            usrOrder.addToItems(newItem)
            usrOrder.save(flush: true)
        }
        redirect(controller: "makeOrder", action: "index")
    }


    def departmentChange(long departmentId) {
        Department department = Department.get(departmentId)
        def workers = []
        if (department != null) {
            workers = department.employers
        }
        render g.select(id: 'workers', name: 'worker.id',
                from: workers, optionKey: 'id', noSelection: [null: 'Выберите работника']
        )
    }
}
